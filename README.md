## **VAGRANT auto-provisioned demo stand for HAProxy with automatic obtaining of certificates from Let's Encrypt**

* Deploy HAProxy node with variable amount of backends
* Deploy variable amount of httpd nodes
* Automaticaly obtain [Let's Encrypt](https://letsencrypt.org/) sertificates
* Auto renew sertificates