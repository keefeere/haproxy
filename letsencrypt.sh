#!/bin/sh -eux


DOMAIN='keefeere.tk'
MAIL='keefeere@ukr.net'


sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s -f /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly --standalone --preferred-challenges http -d $DOMAIN --non-interactive --agree-tos --email $MAIL --http-01-port=8888
sudo mkdir -p /etc/haproxy/certs
sudo rm -f /etc/haproxy/certs/$DOMAIN.pem
sudo cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/haproxy/certs/$DOMAIN.pem
sudo sed -iE "17i\  bind *:443 ssl crt /etc/haproxy/certs/$DOMAIN.pem\n\  redirect scheme https code 301 if !{ ssl_fc }" /etc/haproxy/haproxy.cfg
sudo service haproxy restart
sudo cp /vagrant/renew.sh /usr/local/bin/renew.sh
sudo chmod +x /usr/local/bin/renew.sh
echo "5 7,17 * * * root /bin/sh -c '/usr/local/bin/renew.sh | /usr/bin/logger -t certbot'" | crontab -u root -