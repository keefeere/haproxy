#!/bin/sh -eux


DOMAIN='keefeere.tk'
MAIL='keefeere@ukr.net'

sudo certbot certonly --standalone --preferred-challenges http -d $DOMAIN --non-interactive --agree-tos --email $MAIL --http-01-port=8888
sudo mkdir -p /etc/haproxy/certs
sudo rm -f /etc/haproxy/certs/$DOMAIN.pem
sudo cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/haproxy/certs/$DOMAIN.pem
sudo service haproxy restart